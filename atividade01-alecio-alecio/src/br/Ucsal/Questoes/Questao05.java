package br.Ucsal.Questoes;

import java.util.Scanner;

public class Questao05 {
	private static Scanner sc;
	public static void main(String[] args) {
		onInicio();
	}
	public static void onInicio() {
		int x = onLerInt(onDeclararInt());
		double y = onRepeticao(x);
		onImprimir(y);
	}
	public static int onDeclararInt() {
		int num = 0;
		return num;
	}
	public static int onLerInt(int num) {
		System.out.println("Digite um valor inteiro: ");
		sc = new Scanner(System.in);
		num = sc.nextInt();
		return num;
	}
	public static double onFatorialInverso(int num) {
		if(num==0) return 1;
		else if(num==1) return 1;
		else return num*onFatorialInverso(num-1);
	}
	public static double onRepeticao(int num) {
		double v[] = new double[num+1];
		double v2[] = new double[num+1];
		int x=0;
		for (int i = 0; i < v.length; i++) {
			v[i] = onFatorialInverso(x);
			x+=1;
		}
		double aux = 0;
		for (int i = 0; i < v.length; i++) {
			aux+=1/v[i];
			v2[i] = aux;
		}
		return aux;
	}
	public static void onImprimir(double aux) {
		System.out.println("A soma dos fatoriais inversos desse numero resulta em: ");
		System.out.printf("%.4f", aux);
	}
}
