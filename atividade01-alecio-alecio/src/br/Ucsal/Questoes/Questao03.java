package br.Ucsal.Questoes;

import java.util.Scanner;

public class Questao03 {
	private static Scanner sc;
	public static void main(String[] args) {
		onInicio();
	}
	public static void onInicio() {
		int n = onLerNota(onDeclararNota());
		onCondicao(n);
	}
	public static int onDeclararNota() {
		int nota = 0;
		return nota;
	}
	public static int onLerNota(int nota) {
		System.out.println("Digite a nota do aluno: ");
		sc = new Scanner(System.in);
		nota = sc.nextInt();
		return nota;
	}
	public static void onCondicao(int n) {
		if(n>=0 && n<=49) {
			System.out.println("Aluno insuficiente");
		}
		else if(n>=50 && n<=64) {
			System.out.println("Aluno Regular");
		}
		else if(n>=65 && n<=84) {
			System.out.println("Aluno Bom");
		}
		else if(n>=85 && n<=100) {
			System.out.println("Aluno Otimo");
		}
	}
 }
